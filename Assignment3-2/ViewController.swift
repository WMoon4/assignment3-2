//
//  ViewController.swift
//  Assignment3-2
//
//  Created by V.K. on 2/9/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        insertionSortAndDuplicatesRemoval(arr: [8, 9, 1, 2, 8, 5, 1, 7])
        
        
        ukrainianToEnglish(str: "юбаҐал$аЩанип'ю")
        
        
        let searchStr = "too"
        let textArr: [String] = ["I",
                                 "understood",
                                 "it",
                                 "could",
                                 "be ",
                                 "useful",
                                 "tool",
                                 "too"]
        
        findAndSortMatchingStrings(str: searchStr, arr: textArr)
        
        
        let rawText = "AYFKMWTS is abbreviation of: Are You F***ing Kidding Me With This Sh** F"
        
        cleanThatS___Out(text: rawText)
    
    }

    func insertionSortAndDuplicatesRemoval(arr: [Int]) {
        var arr = arr
        print("---------- 7 ----------")
        print("Original array:")
        for el in arr {
            print(el)
        }
        if arr.count < 2 {
            print("apparently sorted!")
            return
        }
        
        // insertion sort with duplicates removal
        var j = 1
        var arrCount = arr.count
        while j < arrCount {
            let key = arr[j]
            var i = j - 1
            while i >= 0 && arr[i] > key {
                arr[i + 1] = arr[i]
                i -= 1
            }
            if i >= 0 && arr[i] == key {
                // found duplicate
                arr.remove(at: i + 1)
                arrCount -= 1  // cut down upper bound
            } else {
                // normal insert key
                arr[i + 1] = key
                j += 1
            }
            
        }
        
        // print out
        print("Unique values and sorted:")
        for el in arr {
            print(el)
        }
    }
    
    let translit: [String:[String]] = ["а":["a"],
                                     "б":["b"],
                                     "в":["v"],
                                     "г":["h"],
                                     "ґ":["g"],
                                     "д":["d"],
                                     "е":["e"],
                                     "є":["ye", "ie"],  // ye - beginning, ie - elsewhere
                                     "ж":["zh"],
                                     "з":["z"],
                                     "и":["y"],
                                     "і":["i"],
                                     "ї":["yi", "i"],  // yi - beginning, i - elsewhere
                                     "й":["y", "i"],   // y - beginning, i - elsewhere
                                     "к":["k"],
                                     "л":["l"],
                                     "м":["m"],
                                     "н":["n"],
                                     "о":["o"],
                                     "п":["p"],
                                     "р":["r"],
                                     "с":["s"],
                                     "т":["t"],
                                     "у":["u"],
                                     "ф":["f"],
                                     "х":["kh"],
                                     "ц":["ts"],
                                     "ч":["ch"],
                                     "ш":["sh"],
                                     "щ":["shch"],
                                     "ю":["yu", "iu"],  // yu - beginning, iu - elsewhere
                                     "я":["ya", "ia"],  // ya - beginning, ia - elsewhere
                                     "ь":[""],
                                     "'":[""]]
    
    func ukrainianToEnglish(str: String) {
        print("---------- 8 ----------")
        print("Original string: \(str)")
        
        var newStr = ""
        for index in str.indices {
            let orig = String(str[index]).lowercased()
            var dest = ""
            if let optArr: [String] = translit[orig] {
                if optArr.count > 1 {
                    dest = (index == str.startIndex) ? optArr[0] : optArr[1]
                } else {
                    dest = optArr[0]
                }
            }
            if str[index].isUppercase {
                dest = dest.capitalized
            }
            newStr += dest
        }
        print("UA->EN transliterated string: \(newStr)")
    }
    
    func findAndSortMatchingStrings(str: String, arr: [String]) {
        print("---------- 9 ----------")
        print("Search pattern: \(str)")
        print("Original array:")
        for el in arr {
            print(el)
        }
        
        var arr1 = arr.filter({ $0.contains(str) })
        //arr1.sort()
        arr1.sort(by: <)
        
        print("Filtered and sorted array:")
        for el in arr1 {
            print(el)
        }
    }
    
    func cleanThatS___Out(text: String) {
        let list: Set = ["f***", "sh**"]
        print("---------- 10 ----------")
        print("Search patterns:")
        for el in list {
            print(el)
        }
        print("Original text: \(text)")
        
        var originalText = text
        let text = text.lowercased()
        for index in text.indices {
            for el in list {
                if text[index] == el[el.startIndex] {
                    if let endOfRange = text.index(index, offsetBy: el.count, limitedBy: text.endIndex) {
                        let range = index..<endOfRange
                        if text[range] == el {
                            originalText.removeSubrange(range)
                            originalText.insert(contentsOf: String(repeating: "=", count: el.count),
                                                at: index)
                        }
                    }
                }
            }
        }
        print("Sanitized text: \(originalText)")
        
    }
}

